const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
//const express = require('express');
const sqlite3 = require('sqlite3').verbose();

const app = express();
const port = 3001;

// Middleware
app.use(cors());
app.use(bodyParser.json());

// Connexion à la base de données SQLite
const db = new sqlite3.Database('./src/back/base.db', (err) => {
    if (err) {
        console.error('Erreur lors de l\'ouverture de la base de données', err.message);
    } else {
        console.log('Connexion à la base de données SQLite établie');
    }
});

// Exemple de route pour récupérer des données depuis la base de données
app.get('/login', (req, res) => {
    const email = req.query.email;

    if (email) {
        const query = 'SELECT * FROM login WHERE identifiant = ?';

        db.all(query, [email], (err, rows) => {
            if (err) {
                console.error('Erreur SQL:', err);
                res.status(500).json({ error: 'Erreur interne du serveur' });
                return;
            }

            res.json(rows);
        });
    } else {
        res.status(400).json({ error: 'Aucun email fourni dans la requête' });
    }
});





// Fermer la connexion à la base de données lorsque le serveur est arrêté
app.on('close', () => {
    db.close((err) => {
        if (err) {
            console.error('Erreur lors de la fermeture de la base de données', err.message);
        } else {
            console.log('Connexion à la base de données SQLite fermée');
        }
    });
});


// Routes
app.get('/', (req, res) => {
    res.send('Backend is running ein');
});

// Start server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
